#!/usr/bin/env python3
# filewriteasync.py

# REF: https://realpython.com/async-io-python/

from pathlib import Path
import asyncio
import aiofiles


async def file_writer(filename, chunk):
    print("file_writer start")

    async with aiofiles.open(filename, mode="a") as out:
        await out.write(chunk + "\n")
        await out.flush()

    print("file_writer end")
    print("file size: " + str(Path(filename).stat().st_size))  # prints file size


async def other():
    print("do other stuff here", flush=True)
    pass


async def main():
    filename = "foo.txt"
    chunk = "a" * ((10 ** 9) - 2)
    await asyncio.gather(file_writer(filename, chunk), other())


if __name__ == "__main__":
    import time

    start_time = time.perf_counter()
    asyncio.run(main())
    elapsed = time.perf_counter() - start_time
    print(f"{__file__} executed in {elapsed:0.2f} seconds.")
